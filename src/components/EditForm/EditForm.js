import React, {useEffect, useState} from 'react';
import './EditForm.css';
import axiosApi from "../../axiosApi";

const EditForm = ({alias, currentPage, onInputChange, onSelectChange, saveChanges}) => {
    const [pages, setPages] = useState({});

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi('pages.json');
            setPages(response.data);
        };

        fetchData();
    }, [])

    return (
        <form onSubmit={saveChanges} className="EditForm">
            <label htmlFor="pagename">Page name</label>
            <select name="alias" id="alias" onChange={onSelectChange} value={alias}>
                <option key="0" value="0">Choose page</option>
                {Object.keys(pages).map(alias => {
                    return <option key={alias} value={alias}>{pages[alias].title}</option>
                })}
            </select>
            <label htmlFor="title">Title</label>
            <input
                className="Input"
                type="text"
                name="title"
                placeholder="Page title"
                value={currentPage.title}
                onChange={onInputChange}
            />
            <label htmlFor="content">Page text</label>
            <textarea
                className="Textarea"
                name="content"
                placeholder="Page content"
                value={currentPage.content}
                onChange={onInputChange}>{currentPage.content}</textarea>
            <button type="submit">SAVE</button>
        </form>
    );
};

export default EditForm;