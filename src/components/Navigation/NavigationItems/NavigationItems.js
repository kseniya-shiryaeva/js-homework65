import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";
import './NavigationItems.css';

const NavigationItems = () => {
    return (
        <ul className="NavigationItems">
            <NavigationItem to="/" exact>Home</NavigationItem>
            <NavigationItem to="/pages/about">About</NavigationItem>
            <NavigationItem to="/pages/services">Services</NavigationItem>
            <NavigationItem to="/pages/portfolio">Portfolio</NavigationItem>
            <NavigationItem to="/pages/price">Price</NavigationItem>
            <NavigationItem to="/pages/contacts">Contacts</NavigationItem>
            <NavigationItem to="/pages/admin">Admin</NavigationItem>
        </ul>
    );
};

export default NavigationItems;