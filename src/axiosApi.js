import axios from "axios";

const axiosApi = axios.create({
  baseURL: 'https://burger-kit-naviu-default-rtdb.europe-west1.firebasedatabase.app/admin'
});

export default axiosApi;