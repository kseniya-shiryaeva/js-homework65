import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import PageAdmin from "./containers/PageAdmin/PageAdmin";
import Page from "./containers/Page/Page";
import HomePage from "./containers/HomePage/HomePage";

function App() {
  return (
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route path="/" exact component={HomePage}/>
            <Route path="/pages/admin" component={PageAdmin}/>
            <Route path="/pages/:alias" component={Page}/>
            <Route render={() => <h1>Not found</h1>}/>
          </Switch>
        </Layout>
      </BrowserRouter>
  );
}

export default App;
