import React, {useEffect, useState} from 'react';
import './Page.css';
import axiosApi from "../../axiosApi";

const Page = ({match}) => {
    const [page, setPage] = useState(null);

    useEffect(() => {
        const getText = async () => {
            const request = await axiosApi('pages/' + match.params.alias + '.json');
            setPage(request.data);
        };
        getText();
    }, [match.params.alias]);

    let content = 'Текст недоступен';

    if (page) {
        content = (
            <>
                <h1>{page.title}</h1>
                <div>{page.content}</div>
            </>
        );
    }

    return (
        <div className="Page">
            {content}
        </div>
    );
};

export default Page;