import React, {useEffect, useState} from 'react';
import './PageAdmin.css';
import EditForm from "../../components/EditForm/EditForm";
import axiosApi from "../../axiosApi";

const PageAdmin = ({history}) => {
    const [currentPage, setCurrentPage] = useState({
        'title': '',
        'content': ''
    });

    const [alias, setAlias] = useState(0);

    useEffect(() => {
        const getCurrentText = async (e) => {
            if (alias) {
                const editionPage = await axiosApi('pages/' + alias + '.json');

                setCurrentPage(editionPage.data);
            }
        };

        getCurrentText();
    }, [alias]);

    const saveChanges = async e => {
        e.preventDefault();

        try {
            await axiosApi.put('pages/' + alias + '.json', currentPage);
        } finally {
            history.replace('/pages/' + alias);
        }
    }

    const onInputChange = e => {
        const {name, value} = e.target;

        setCurrentPage(prev => ({
            ...prev,
            [name]: value
        }))
    };

    const onSelectChange = async (e) => {
        setAlias(e.target.value);
    };

    return (
        <div className="PageAdmin">
            <EditForm onInputChange={e => onInputChange(e)} saveChanges={saveChanges} onSelectChange={onSelectChange} currentPage={currentPage} alias={alias} />
        </div>
    );
};

export default PageAdmin;