import React from 'react';
import './HomePage.css';

const HomePage = () => {
    return (
        <div className="HomePage">
            <h1>Главная страница</h1>
            <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, debitis deserunt dignissimos
                distinctio, dolorum eligendi fugit iste magni minus modi, molestiae neque nulla odio omnis voluptatum.
                Atque beatae consectetur cumque deleniti dicta doloremque esse id in, iste laudantium magni maxime minus
                natus necessitatibus nemo nulla officiis perspiciatis quaerat quam quos ratione, repellat repellendus,
                repudiandae sed totam ut voluptates. Architecto iste nisi possimus ratione sint ut. Adipisci aliquid
                aut, deserunt distinctio dolorem esse eum excepturi explicabo ipsa nam obcaecati rem sapiente sed, sint
                veniam? Aperiam corporis debitis dolore fugit impedit ipsum iste iusto modi, non, odit omnis quidem quod
                sed vitae.
            </div>
        </div>
    );
};

export default HomePage;